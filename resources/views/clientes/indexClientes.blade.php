@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row align-items-center justify-content-between">
                        <div class="col">
                            <h1><i class="bi bi-people-fill"></i> Clientes</h1>
                        </div>
                        <div class="col text-end">
                            <a href="{{ route('clientes.create') }}" class="btn btn-lg btn-primary">
                                Novo <i class="bi bi-plus-lg"></i></a>
                        </div>
                    </div>
                   
                    @if ($clientes->isEmpty())
                        <div class="my-3 text-center opacity-75">
                            <hr class="mb-4">
                            Nenhum usuario cadastrado...
                        </div>
                    @else

                    <div class="border rounded p-3">
                        <table id="clientesTable" class="table table-hover">
                            <thead>
                                <tr class="fs-5">
                                    <th scope="col"></th>
                                    <th scope="col">Nome</th>
                                    <th scope="col">Telefone</th>
                                    <th scope="col">Endereço</th>
                                    <th scope="col"></th>

                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clientes as $cliente)
                                    <tr id="{{ $cliente->id }}" class="align-middle">
                                        <th class="py-3 text-center ">{{ $clientes->firstItem() + $loop->index }}</th>
                                        <td class="py-3"> {{ $cliente->nome }}</td>
                                        <td class="py-3">{{ $cliente->telefone }}</td>
                                        <td class="py-3">{{ $cliente->endereco }}</td>
                                        <td class="py-3 text-end">
                                            <a href="{{ route('clientes.edit', ['cliente' => $cliente->nome]) }}" class="btn btn-info text-light">
                                                <i class="bi bi-pencil-square"></i> Editar
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                        
                    @endif

                    <div class="row">
                        {{-- $clientes->links() --}}
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
