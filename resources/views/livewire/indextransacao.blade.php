<div>

    @if (!$transacoes->isEmpty())
        <div class="card mt-4">
            <div class="card-body">

                <div class="col-12 fs-4 fw-bold">
                    Dívidada Total: <span>R$ {{ $cliente->divida_total }}</span>
                </div>
            </div>
        </div>
    @endif


    <div class="card mt-4">
        <div class="card-body">


            <div class="row align-items-center justify-content-between">
                <div class="col-8">
                    <h2 class="mt-2"><i class="bi bi-arrow-left-right"></i> Transações</h2>
                </div>

                <div class="col-4 text-end">
                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalCriarTrsc">
                        Criar <i class="bi bi-plus-lg"></i>
                    </button>

                    <x-transacao.createTransacao></x-transacao.createTransacao>


                    <!-- Ouça o evento 'fecharModal' e feche o modal quando o evento for recebido -->
                    @script
                        <script>
                            $wire.on('fecharModal', () => {
                                // Aciona o clique no botão usando JavaScript
                                document.getElementById('fecharModal').click();
                            });
                        </script>
                    @endscript

                </div>

            </div>
            <hr>

            @if ($transacoes->isEmpty())
                <div class="row opacity-50 text-center">
                    <span class="my-5">Nenhuma transação efetuada</span>
                </div>
            @else
                <div class="row gy-2">

                    @foreach ($transacoes as $transacao)
                        <div class="col-12">
                            <div class="card hoverTransacao" wire:click="view({{ $transacao->id }})"
                                x-bind:class="{
                                    'text-success': '{{ $transacao->tipo }}' == '0',
                                    'text-danger': '{{ $transacao->tipo }}' == '1',
                                }">

                                <div class="card-body row ">
                                    <div class="col-1">{{ $transacao->quantidade }}</div>
                                    <div class="col">{{ $transacao->item }}</div>
                                    <div class="col">{{ $transacao->created_at->format('d/m/Y H:i') }}</div>
                                    <div class="col-2 text-end ">R$ {{ $transacao->valor }}</div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

                <x-transacao.viewTransacao></x-transacao.viewTransacao>

                <button class="d-none" id="viewModalIndex" data-bs-toggle="modal"
                    data-bs-target="#viewTransacaol"></button>
                @script
                    <script>
                        $wire.on('abrirViewModal', () => {
                            // Aciona o clique no botão usando JavaScript
            

                            document.getElementById('viewModalIndex').click();

                        });

                        $wire.on('fecharViewModal', () => {
                            // Aciona o clique no botão usando JavaScript
                            document.getElementById('fecharViewModal').click();
                        });
                    </script>
                @endscript
            @endif

        </div>
    </div>

</div>
