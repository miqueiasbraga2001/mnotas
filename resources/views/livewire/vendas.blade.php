<div>
    <x-caixa  class="mb-4">
        <x-vendas.graficoVenda></x-vendas.graficoVenda>
    </x-caixa>

    <x-caixa>
        <x-slot name="header">
            <x-vendas.formCreateVendas></x-vendas.formCreateVendas>
        </x-slot>

        <x-vendas.filtroIndex></x-vendas.filtroIndex>

        <x-vendas.indexVendas></x-vendas.indexVendas>

    </x-caixa>
</div>
