<div class="row gy-2">
    
    <x-vendas.viewVenda></x-vendas.viewVenda>

    {{-- wire:click="$set('venda',{{ $venda }})"

        A variavel é recibida como um array, então vou trata-la como tal em ViewVenda
        tentei converter assim {{ json_encode($venda) }} e não funcionou...
    --}}

    @foreach ($this->vendas as $venda)
        <div wire:key="{{ $venda->id }}"  class="col-12" >
            <div class="card ItemLista bg-gray-200" wire:click.live="$set('venda',{{ $venda }})" data-bs-toggle="modal" data-bs-target="#viewVenda">
                <div class="card-body row ">
                    <div class="col-1">{{ $venda->quantidade }}</div>
                    <div class="col text-capitalize">{{ $venda->item }}</div>
                    <div class="col"><i class="bi bi-clock"></i> {{ $venda->created_at->format('H:i') }}</div>
                    <div class="col-2 text-end ">R$ {{ $venda->valor }}</div>
                </div>
            </div>
        </div>
    @endforeach

    <button id="btnRefresh" class="d-none" wire:click="$refresh">Recarrega LISTA</button>

    @script
        <script>
            $wire.on('recarregaLista', () => {
                document.getElementById('btnRefresh').click()
            })
        </script>
    @endscript
</div>
