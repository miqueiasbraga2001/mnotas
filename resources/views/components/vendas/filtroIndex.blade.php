<div class="row my-3 justify-content-between align-items-end">
    <div class="col-lg-2 col-md-4 col-6">
        <input class="form-control form-control-lg {{ $this->dataAtual == now()->format('Y-m-d') ? 'text-success' : 'text-danger' }}" id="campoData" type="date" wire:model.live="dataAtual"
            max="{{ now()->format('Y-m-d') }}">        
    </div>

    <div class="col-lg-3 col-md-4 col-6 text-end">
        <button class="btn btn-light w-100" disabled>
            <span class="fs-4"><i class="bi bi-wallet2"></i> R$ {{ $this->vendaTotal }}</span>
        </button>
    </div>
  

</div>
