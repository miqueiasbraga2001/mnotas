<!-- Modal -->

    <div wire:ignore.self class="modal fade" id="viewVenda" tabindex="-1" aria-labelledby="viewVendaLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">


                <div class="modal-header">
                    <h1 class="modal-title fs-5" id="viewVendaLabel">Venda</h1> 
                    
                    {{-- neste caso alem de verificar se é null verifico tambem se é 'Vazia' --}}
                    <small class="ms-2">
                        {{ !empty($this->venda['created_at']) ? (new DateTime($this->venda['created_at']))->format('d/m/Y - H:i') : '' }}
                    </small>
                    
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>


                <div class="modal-body row">

                    <h4 class="col-12 mb-4"> {{ $this->venda['item'] ?? '' }}</h4>

                    <div class="col-6">
                        <span>Quantidade: {{ $this->venda['quantidade'] ?? '' }}</span>
                    </div>
                    <div class="col-6 text-end">
                        <h6>R$ {{ $this->venda['valor'] ?? '' }}</h6>
                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-danger" wire:click="destroy()" data-bs-dismiss="modal">Excluir</button>
                </div>
            </div>
        </div>
    </div>

