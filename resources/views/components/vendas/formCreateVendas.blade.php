<form wire:submit.prevent="save">
    <div class="row align-items-center gy-md-0 gy-3 my-2">
        <div class="col-lg-1 col-md-2 col-6 order-md-1 order-2"> 
            <input id="inputQuantidade" type="number" class="form-control @error('quantidade') is-invalid @enderror"
                wire:model="quantidade">
            @error('quantidade')
                <span class="invalid-feedback">Minimo 1</span>
            @enderror
        </div>

        <div class="col-lg-8 col-md-5 col-12 order-md-2 order-1">
            <input id="inputItem" required type="text" class="form-control text-capitalize @error('item') is-invalid @enderror"
                placeholder="Item" wire:model="item">
            @error('item')
                <span class="invalid-feedback">Descrição Obrigatória</span>
            @enderror
        </div>

        <div class="col-lg-2 col-md-3 col-6 order-md-3 order-3">
            <div class="input-group">
                <div class="input-group-text">R$</div>
                <input id="inputValor" placeholder="0,00" required type="text" class="form-control @error('valor') is-invalid @enderror"
                    x-mask:dynamic="$money($input, ',')" wire:model="valor">
            </div>
            @error('valor')
                <span class="invalid-feedback">Valor menor ou igual a 0</span>
            @enderror
        </div>

        <div class="col-lg-1 col-md-2 col-12 order-md-4 order-4">
            <button type="submit" class="btn btn-success w-100">
                <i class="bi bi-plus-lg"></i>
            </button>
        </div>
    </div>
</form>


@script
    <script>
        $wire.on('clearFormCreateVenda', () => {
            document.getElementById('inputValor').value = '';
            document.getElementById('inputQuantidade').value = 1;
            document.getElementById('inputItem').value = '';
        });
    </script>
@endscript
