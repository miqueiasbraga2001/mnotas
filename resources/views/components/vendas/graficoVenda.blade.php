<div class="row">
    <div class="col-12">

        <canvas wire:key="graficoDia" wire:ignore.self width="800" height="100" id="myChart"></canvas>
       
    </div> 
</div>
@script
    <script>
        let myChart; // Declare a variável fora do escopo da função para que seja acessível em todas as chamadas

        $wire.on('atualizaGrafico', function(dados) {
            dados = JSON.parse(dados[0].dados)
            const ctx = document.getElementById('myChart');

            // Destrói o gráfico existente se ele já foi criado
            if (myChart) {
                myChart.destroy();
            }

            // Cria uma nova instância do gráfico com os dados atualizados
            myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: {!! $this->IntervaloHorasGrafico !!},
                    datasets: [{
                        label: 'Venda Diária',
                        data: dados,
                        borderWidth: 1,
                        fill: true,
                        backgroundColor: 'rgba(75, 192, 192, 0.2)',
                        borderColor: 'rgba(75, 192, 192, 1)',
                        tension: 0.3
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
        });
    </script>
@endscript
