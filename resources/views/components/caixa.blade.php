<div {{ $attributes->merge(['class' => 'card px-0 shadow-sm'])}} >

    @if ($header ?? false)
        <div class="card-header">
            {{ $header }}
        </div>
    @endif
    <div class="card-body">
        {{ $slot }} 
    </div>
</div>
