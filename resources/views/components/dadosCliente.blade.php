<div>
    <div class="row align-items-center justify-content-between">
        <div class="col-lg-10 col-md-9 col-sm-8">
            <h1><i class="bi bi-person-fill"></i> {{ $cliente->nome }}</h1>
        </div>

        <div class="col-lg-2 col-md-3 col-sm-4 text-end">
            <div class="row g-2">
                <div class="col">

                    <!-- Button trigger modal -->
                    <button type="button" class="w-100 btn btn-danger" data-bs-toggle="modal"
                    data-bs-target="#aviso2">
                        Deletar
                    </button>

                    <x-modal.deleteCliente :cliente="$cliente"></x-modal.deleteCliente>

                </div>
                <div class="col">
                    <button class="w-100 btn btn-info text-light"
                        href="{{ route('clientes.edit', ['cliente' => $cliente->nome]) }}">Editar</button>
                </div>
            </div>
        </div>
    </div>

    <hr class="opacity-25">

    <div class="row align-items-center justify-content-between mt-4 gy-md-4">
        <div class="col-lg-4 col-md">
            <h5><i class="bi bi-telephone-fill"></i> {{ $cliente->telefone }}</h5>
        </div>
        <div class="col-lg-4 col-md">
            <h5><i class="bi bi-hash"></i> {{ $cliente->cpf }}</h5>
        </div>
        <div class="col-lg-4 col-md">
            <h5><i class="bi bi-geo-fill"></i> {{ $cliente->endereco }}</h5>
        </div>
    </div>

    <div class="row align-items-center justify-content-between mt-2 gy-md-4">
        <p class="col-lg-8"><i class="bi bi-file-earmark-text"></i> {{ $cliente->descricao }}</p>
    </div>

    <div class="text-start">
        <small>Criado em: {{ $cliente->created_at->format('d/m/Y H:i') }}</small>
    </div>
</div>
