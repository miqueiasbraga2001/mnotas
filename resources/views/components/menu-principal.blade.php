<nav class="navbar navbar-expand-md bg-success   shadow-sm">
    <div class="container">
        <a class="navbar-brand text-light" href="{{ url('/') }}">
            {{ config('app.name', 'MNotas') }}
        </a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            @auth
                <ul class="navbar-nav me-auto " x-data="{
                    'linkAtivo': 'text-light fw-bold border-bottom border-2',
                    'link': 'text-light'
                }" >
                    <li class="nav-item">
                        <a
                            class="nav-link {{ request()->routeIs('home') ? 'text-light fw-bold border-bottom border-2' : 'text-light' }}" href="{{ route('home') }}">{{ __('Painel') }}</a>
                    </li>

                    {{--  ------------------------ Só na proxima versão...
                    <li class="nav-item">
                        <a
                            class="nav-link   {{ request()->routeIs('clientes.index') ? 'text-light fw-bold border-bottom border-2' : 'text-light' }}" href="{{ route('clientes.index') }}">{{ __('Clientes') }}</a>
                    </li>
                    --}}
                </ul>
            @endauth

 
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ms-auto ">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item ">
                            <a class="nav-link link-light" href="{{ route('login') }}">{{ __('Entrar') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link link-light" href="{{ route('register') }}">{{ __('Cadastrar-se') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle text-white" href="#" role="button"
                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Sair') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
