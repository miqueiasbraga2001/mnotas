<!-- Button trigger modal -->
<div wire:ignore.self class="modal fade" id="viewTransacaol" tabindex="-1" aria-labelledby="viewTransacaolLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="viewTransacaolLabel">
                    {{ !is_null($this->transacao) ? $this->transacao->item : '' }}</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body row g-4">
                <div class="col-6">
                    Valor: R$ {{ !is_null($this->transacao) ? $this->transacao->valor : '' }}
                </div>
                <div class="col-6 text-end">
                    Data: {{ !is_null($this->transacao) ? $this->transacao->created_at->format('d/m/Y H:i') : '' }}
                </div>
                {{-- Corrigir Lógica
                  <div class="col-6">
                    Dívida anterior: R$ {{ !is_null($this->transacao) ? $this->somaTransacoesAnteriores : '' }}
                  </div> --}}
            </div>
            <div class="modal-footer">
                <button type="button"  id="fecharViewModal"  wire:click="$refresh" class="btn btn-secondary"
                    data-bs-dismiss="modal">Sair</button>

                <button type="button" class="btn btn-danger" wire:click="destroy()">Deletar</button>

            </div>
        </div>
    </div>
    
</div>
