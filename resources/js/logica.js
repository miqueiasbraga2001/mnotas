document.addEventListener('DOMContentLoaded', function() {

   // Obtendo referência para o elemento
   var todasAsCelulas = document.querySelectorAll("#clientesTable tbody td");

   // Adicionando ouvinte de evento de clique a cada célula
   todasAsCelulas.forEach(function(celula) {
    celula.addEventListener("click", function() {
       // Obtendo o ID do tbody pai
       var idTr = celula.closest('tr').id;

      // alert("ID do tbody pai: " + idTr);

       window.location.href = "/clientes/" + idTr;
      // Adicione aqui o código que deseja executar quando uma célula for clicada
    });
  });

  
})
 