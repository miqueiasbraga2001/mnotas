<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\TransacaoController;



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Rotas protegidas por autenticação
Route::middleware(['auth'])->group(function () {
    Route::get('/vendas', [App\Http\Controllers\HomeController::class, 'index'])->name('vendas');
    Route::resource('clientes', ClienteController::class);
    Route::resource('transacao', TransacaoController::class);
});

/*
Route::prefix('clientes')->group(function () {
    Route::get('/', [ClienteController::class, 'index'])->name('clientes');
    Route::get('/{cliente}', [ClienteController::class, 'view'])->name('clientes.view');
    Route::get('/criar', [ClienteController::class, 'create'])->name('clientes.create');
    Route::get('/editar/{cliente}', [ClienteController::class, 'edit'])->name('cliente.edit');

    Route::post('/store', [ClienteController::class, 'store'])->name('clientes.store');
    Route::put('/update/{cliente}', [ClienteController::class, 'update'])->name('clientes.update');
});*/