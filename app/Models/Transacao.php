<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transacao extends Model {
    use HasFactory;

    protected $table = 'transacaos'; // Nome da tabela
    protected $fillable = [
        'cliente_id',
        'item',
        'quantidade',
        'valor',
        'tipo',
    ]; // Colunas que podem ser preenchidas em massa

    // Relacionamento: Um pedido pertence a um usuário
    public function umCliente() {
        return $this->belongsTo(Cliente::class);
    }
}
