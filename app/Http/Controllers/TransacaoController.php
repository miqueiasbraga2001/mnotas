<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TransacaoController extends Controller {
    public function index() {
        return view('transacao.indexTransacao');
    }

    public function show($clienteId) {
    }

    public function store(Request $request) {
        dd($request->tipo);
    }

    public function edit($cliente) {
    }

    public function update(Request $request, $clienteId) {
    }
    public function destroy($cliente) {
    }

    public function massageError() {
        // Mensagens personalizadas
        $mensagens = [
            'nome.required' => 'O campo nome é obrigatório.',
            'nome.string' => 'O campo nome deve ser uma string.',
            'nome.max' => 'O campo nome não pode ter mais de :max caracteres.',
            'nome.unique' => 'O nome já está em uso.',

            'cpf.required' => 'O campo CPF é obrigatório.',
            'cpf.string' => 'O campo CPF deve ser uma string.',
            'cpf.max' => 'O campo CPF não pode ter mais de :max caracteres.',
            'cpf.formato_cpf' => 'O CPF informado não está em um formato válido.',
            'cpf.unique' => 'O CPF já está cadastrado.',

            'telefone.required' => 'O campo telefone é obrigatório.',
            'telefone.telefone_com_ddd' => 'O telefone não está em um formato válido.',
            'telefone.string' => 'O campo telefone deve ser uma string.',
            'telefone.max' => 'O campo telefone não pode ter mais de :max caracteres.',

            'endereco.string' => 'O campo endereço deve ser uma string.',
            'endereco.max' => 'O campo endereço não pode ter mais de :max caracteres.',

            'descricao.string' => 'O campo descrição deve ser uma string.',
        ];

        return $mensagens;
    }
}
