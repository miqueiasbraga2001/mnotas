<?php

namespace App\Livewire;

use App\Models\Cliente;
use App\Models\Transacao;
use Livewire\Component;

class Indextransacao extends Component {

    public $cliente;

    public $transacoes;
    public $transacao;
    public $somaTransacoesAnteriores;
    public $tipo;
    public $item;
    public $quantidade;
    public $valor;

    public function mount($cliente) {

        // O refresh não passa por aqui
        $this->cliente = $cliente;
        $this->tipo = 1;
        $this->quantidade = 1;
        
    }

    public function render() {

        $this->cliente->divida_total = Cliente::findOrfail($this->cliente->id)->divida_total;
        //$transacoes = Transacao::were
        $this->transacoes = Transacao::where('cliente_id', $this->cliente->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('livewire.indextransacao');
    }

    public function save() {

        //Compra = 1
        //Pagamento = 0

        if ($this->tipo) {
            $transacao = Transacao::create([
                'item' => $this->item,
                'valor' => $this->formatValor($this->valor),
                'quantidade' => $this->quantidade,
                'tipo' => $this->tipo,
                'cliente_id' => $this->cliente->id
            ]);
        } else {
            $transacao = Transacao::create([
                'item' => 'Pagamento',
                'valor' => $this->formatValor($this->valor),
                'quantidade' => 1,
                'tipo' => $this->tipo,
                'cliente_id' => $this->cliente->id
            ]);
        }

        // Atualização da divida total do cliente
        $clienteBD = Cliente::findOrFail($this->cliente->id);

        $clienteBD->update([
            'divida_total' => $this->calculaDividaT()
        ]);

         //Reseta os Campos
         $this->item = '';
         $this->tipo = 1;
         $this->quantidade = 1;
         $this->valor = '';
 
      
        // Se o formulário for processado com sucesso, emita o evento para fechar o modal
       $this->dispatch('fecharModal');
    }

    public function view($transacaoId) {

        $this->transacao = Transacao::where('id', $transacaoId)->first();
        //dd( $this->transacao );
        $this->somaTransacoesAnteriores = $this->calculaDividaAnterior($this->transacao->created_at);

        $this->dispatch('abrirViewModal');
        //dd($this->transacao);
        // Se o formulário for processado com sucesso, emita o evento para fechar o modal
        //$this->dispatch('ModalViewTransacao'); 
        //dd($this->transacao->id);
    }

    public function destroy() {

        $this->dispatch('fecharViewModal');

        $transacao = Transacao::find($this->transacao->id);

        $transacao->delete();
        //dd($transacao = Transacao::find($this->transacao->id));
        // Atualização da divida total do cliente


        $clienteBD = Cliente::findOrFail($this->cliente->id);

        $clienteBD->update([
            'divida_total' => $this->calculaDividaTotal()
        ]);
    }

    public function formatValor($stringNumero) {
        // Remover pontos como separadores de milhares
        $stringNumero = str_replace('.', '', $stringNumero);

        // Substituir a vírgula como separador decimal por um ponto
        $stringNumero = str_replace(',', '.', $stringNumero);

        // Converter a string para um número decimal
        $numeroDecimal = floatval($stringNumero);

        // Agora $numeroDecimal contém o valor desejado
        return $numeroDecimal;
    }

    public function calculaDividaT() {

        $valor = $this->formatValor($this->valor) * $this->quantidade;

        if ($this->tipo) { // Compra
            return $this->cliente->divida_total - $valor;
        } else { // Pagamento
            return $this->cliente->divida_total + $valor;
        }
    }
    public function calculaDividaTotal() {

        $transacoes = Transacao::all();

        $soma = 0;

        foreach ($transacoes as $transacao) {
            if (!$transacao->tipo) {
                $soma += $transacao->valor;
            } else {
                $soma -= $transacao->valor;
            }
        }

        return $soma;
    }

    public function calculaDividaAnterior($data) {
        $transacoesAnterioire = Transacao::all()->where('created_at', '<', $data);
        $soma = 0;
        foreach ($transacoesAnterioire as $transacao) {
            if (!$transacao->tipo) {
                $soma += $transacao->valor;
            } else {
                $soma -= $transacao->valor;
            }
        }

        return $soma;
    }
}
