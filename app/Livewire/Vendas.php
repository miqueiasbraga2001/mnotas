<?php

namespace App\Livewire;

use App\Models\Transacao;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

use Livewire\Component;

class Vendas extends Component {

    public $vendas;
    public $item;
    public $quantidade;
    public $valor;

    public $vendaTotal;

    public $dataAtual;
    public $venda;

    //------ GRÁFICO DIÁRIO 
    public $intervalo;
    public $IntervaloHorasGrafico;
    public $vendasPorIntervalo;

    public function mount() {
        $this->quantidade = 1;
        $this->dataAtual = now()->format('Y-m-d');

        $this->upGrafico();
        //dd($this->vendasPorIntervalo);
    }

    public function render() {

        $this->vendas = Transacao::where('cliente_id', 0)
            ->whereDate('created_at', $this->dataAtual)
            ->orderBy('created_at', 'desc')
            ->get();


        $this->calculaVendaTotal();


        return view('livewire.vendas');
    }

    public function save() {


        //Formata o VALOR pra manipulação
        $this->valor = $this->formatValor($this->valor);

        $this->validate([
            'quantidade' => 'required|numeric|min:1',
            'item' => 'required|string|max:255',
            'valor' => 'required|numeric|min:0.1',
        ]);


        //TIPO: Venda === 1
        //Cliente id == 0 para separa-lo dos que possuem um cliente definido

        $venda = Transacao::create([
            'item' => $this->item,
            'valor' => $this->valor,
            'quantidade' => $this->quantidade,
            'tipo' => 1,
            'cliente_id' => 0
        ]);

        // Limpar os campos após o salvamento
        $this->dispatch('clearFormCreateVenda');
        $this->dispatch('recarregaLista');
        $this->upGrafico();
        $this->dataAtual = now()->format('Y-m-d');
    }

    public function view($vendaID) {

        $this->venda = Transacao::find($vendaID)->first();
        dd($this->venda);
        // dd( $this->venda);
        //$this->dispatch('abreModalView');
    }

    public function destroy() {


        $venda = Transacao::find($this->venda['id']);

        $venda->delete();
        $this->upGrafico();
        $this->dispatch('recarregaLista');
    }

    public function formatValor($stringNumero) {
        // Remover pontos como separadores de milhares
        $stringNumero = str_replace('.', '', $stringNumero);

        // Substituir a vírgula como separador decimal por um ponto
        $stringNumero = str_replace(',', '.', $stringNumero);

        // Converter a string para um número decimal
        $numeroDecimal = floatval($stringNumero);

        // Agora $numeroDecimal contém o valor desejado
        return $numeroDecimal;
    }

    public function calculaVendaTotal() {

        $this->vendaTotal = 0;
        foreach ($this->vendas as $venda) {

            $this->vendaTotal += $venda->valor * $venda->quantidade;
        }
    }

    public function updatedDataAtual() {
        $this->vendas = Transacao::where('cliente_id', 0)
            ->whereDate('created_at', $this->dataAtual)
            ->orderBy('created_at', 'desc')
            ->get();

        $this->calculaVendaTotal();

        $this->upGrafico();

        $this->dispatch('recarregaLista');
    }

    //______________________________________________ G R A F I C O __________________________
    public function upGrafico() {
        $this->intervalo = 60; //em minutos
        //Depois colocar para o próprio usuário definir o horario de trabalho

        $this->IntervaloHorasGrafico = $this->gerarIntervaloHoras('08:00', '18:00',   $this->intervalo);
        //Um array onde cada indice corresponde a um intervalo em horas

        $this->vendasPorIntervalo = $this->CalculaVendasPorIntervalo();
        $this->dispatch('atualizaGrafico', ['dados' =>  $this->vendasPorIntervalo]);
    }

    public function gerarIntervaloHoras($horaInicial, $horaFinal, $intervalo) {
        $res = [];

        $horaAtual = strtotime($horaInicial);

        while ($horaAtual <= strtotime($horaFinal)) {
            $res[] = date('H:i', $horaAtual);
            $horaAtual = strtotime('+' . $intervalo . ' minutes', $horaAtual);
        }

        return json_encode($res);
    }

    public function CalculaVendasPorIntervalo() {

        $horas = json_decode($this->IntervaloHorasGrafico);

        $res = [];

        foreach ($horas as $key => $hora) {
            if ($hora !== Arr::last($horas)) { //Não executa o ultimo item pois é o fim do intervalo
                $res[$key] = Transacao::whereDate('created_at', '=', $this->dataAtual)
                ->whereTime('created_at', '>', $hora)
                    ->whereTime('created_at', '<=', $horas[$key + 1])
                    ->sum(DB::raw('quantidade * valor'));
            }
        }

        // Adiciona o novo elemento (1) no início do array
        //Para renderizar corretamente no gráfico
        array_unshift($res, '');

        return json_encode($res);
    }
}
